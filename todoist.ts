import { ItemAddCommand, ItemUpdateCommand } from "./model";
const axios = require("axios").default;

const api_token = process.env.TODOIST_TOKEN;
const api_endpoint = "https://api.todoist.com/sync/v9/sync";

export function addItem(command: ItemAddCommand) {
  console.log(command);
  axios
    .post(api_endpoint, {
      token: api_token,
      resource_types: '["projects", "items"]',
      commands: JSON.stringify([command])
    })
    .then(function (data) {
      // console.log(data);
    })
    .catch(function (error) {
      console.log(error);
    });
}

export function updateItems(
  sync_token: string,
  item_commands: Array<ItemUpdateCommand>
): void {
  if (item_commands.length === 0) {
    console.log("Nothing to update.");
    return;
  }
  console.log("updating items");
  console.log(item_commands);
  axios
    .post(api_endpoint, {
      token: api_token,
      sync_token: sync_token,
      resource_types: '["items"]',
      commands: JSON.stringify(item_commands)
    })
    .then(function (data) {
      // console.log(data);
    })
    .catch(function (error) {
      console.log(error);
    });
}

export function getItems(ondata_callback: (res) => void): void {
  axios
    .post(api_endpoint, {
      token: api_token,
      sync_token: "*",
      resource_types: '["items"]'
    })
    .then(ondata_callback)
    .catch(function (error) {
      console.log(error);
    });
}
