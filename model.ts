export interface IResponsePayload {
    message: string;
    event: any;
}

export interface IQueryParameters {
    foo: string;
}

export interface IEventPayload {
    body?: string;
    version?: string;
    id?: string;
    "detail-type"?: string;
    source?: string;
    account?: string;
    time?: string;
    region?: string;
    resources?: string[];
    detail?: object;
}

export interface ICallback {
    (error: any, result: IResponsePayload): void;
}

export enum Priority { NONE = 1, LOW, MEDIUM, HIGH }

export enum Toggle { OFF, ON }

export enum Choice { NO, YES }

export interface DueDate {
    date: string;
    timezone?: string;
    string: string;
    lang: string;
    is_recurring: boolean
}

export interface Item {
    id: string;
    section_id?: string;
    description?: string;
    user_id?: string;
    project_id?: string;
    content: string;
    priority: Priority;
    due?: DueDate;
    parent_id?: string;
    child_order?: number;
    day_order?: number;
    collapsed?: Toggle;
    children?: null;
    labels?: [string];
    added_by_uid?: string;
    assigned_by_uid?: string;
    responsible_uid?: string;
    checked?: Choice;
    in_history?: Choice;
    is_deleted?: Choice;
    sync_id?: number;
    added_at?: Date
}

export interface ItemsResponse {
    full_sync: boolean;
    sync_token: string;
    temp_id_mapping: object;
    items: [Item]
}

export interface ItemUpdateCommand {
    type: "item_update";
    uuid: string;
    args: Item
}

export interface ItemAddCommand {
    type: "item_add";
    uuid: string;
    args: Item
}
