```
 _    _       _                   
| |  | |     | |                  
| |  | |_ __ | | _____  ___ _ __  
| |  | | '_ \| |/ / _ \/ _ \ '_ \ 
| |__| | |_) |   <  __/  __/ |_) |
 \____/| .__/|_|\_\___|\___| .__/ 
       | |                 | |    
       |_|                 |_|    
```

# Introduction
This is a few dozen lines of code that saved my marriage. How did it do that? The only thing we ever fought about was chores around the house. We had the classic husband/wife imbalance where I wasn't pulling my weight and I couldn't see it. Being an engineer, I thought I'd take a stab at solving it with software. Upkeep is a very thin integration with [Todoist](https://todoist.com/) that does two things:

1. keeps track of an item's age, and makes its priority increase when it gets too old
2. when a spouse checks off an item, it automatically re-creates it with a fresh timestamp and assigns it to the _other_ spouse

The original project was in JavaScript and integrated with my beloved Wunderlist, but since that was discontinued, I re-wrote it for Todoist, which has been working out great. I switched the language to TypeScript, which (I think) is just enough guardrails to make JavaScript make sense.

It's entirely serverless, using AWS Lambda, and it costs $0.51 per month to run it.

# Setup

1. clone this repo
2. install [serverless framework](https://serverless.com)
3. `npm install`
4. `serverless login` (I used my GitHub account)
5. `serverless --org=dysmento`

# Tests

```
npm test
npm run coverage
```

# Package / Deploy

```
serverless deploy
```
After deploy, you /must/ go to the Lambda console and set the environment
variable for /both/ lambdas: `TODOIST_TOKEN`. You can get the right value from
https://developer.todoist.com/appconsole.html (look for "Test token")


# Todo

I switched to the official Todoist SDK for the reset function. I should do the
same thing for the aging function (still called hello!).

Figure out how to get the API key from a secrets manager somewhere.

Time to write it in Clojure???

