import { targetAge, singleItemUpdate } from '../age-util';

test('target age for 7d should be 604,800,000', () => {
    expect(targetAge('take out the trash 7d')).toBe(604800000);
});

test('command should be null if we have the wrong project', () => {
    expect(singleItemUpdate({id: "1234", project_id: "1001", content: 'Make some cake batter', priority: 1})).toBeNull();
});
