import { ItemsResponse, Item, ItemUpdateCommand, Priority, DueDate } from './model'
import * as _ from 'lodash';
import { v4 as uuid } from 'uuid';

const timeExpression = /(\d+)([hdwm])/;
const HOUR = 60 * 60 * 1000;
const DAY = 24 * HOUR;
const WEEK = 7 * DAY;
const MONTH = 30 * DAY;

export function targetAge(title: string): number {
    const match = timeExpression.exec(title);
    if (match && match.length > 2) {
        switch (match[2]) {
            case 'h':
                return HOUR * +match[1];
            case 'd':
                return DAY * +match[1];
            case 'w':
                return WEEK * +match[1];
            case 'm':
                return MONTH * +match[1];
            default:
                return -1;
        }
    } else {
        return -1;
    }
}

function dueDate(d: Date): DueDate {
    const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d)
    const mo = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(d)
    const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d)
    const monthName = new Intl.DateTimeFormat('en', { month: 'short' }).format(d)
    return {
        date: `${ye}-${mo}-${da}`,
        string: `${monthName} ${da}`,
        lang: "en",
        is_recurring: false,
        timezone: null
    };
}

function ageTask(content: string, priority: Priority, age: number, targetAge: number, id: string, dateAdded: Date): Item {
    console.log(content);
    const expireTime = targetAge > 0 ? dateAdded.getTime() + (targetAge * 1.5) : null;
    if (expireTime && age > targetAge * 0.7) {
        console.log(`Available: YES`);
        content = content.replace('✅ ', '');
        priority = Priority.LOW;
    }

    if (expireTime && age > targetAge * 1.5) {
        console.log(`Biohazard: YES`);
        priority = Priority.HIGH;
        if (!content.startsWith('☣️')) {
            content = '☣️ ' + content;
        }
    }

    return {
        content: content,
        priority: priority,
        id: id,
        due: expireTime ? dueDate(new Date(expireTime - DAY)) : null
    }
}

export function singleItemUpdate(item: Item): ItemUpdateCommand {
    if (item.project_id === "2232783325") {
        console.log("UPDATE: " + item); 
        const tAge = targetAge(item.content);
        const dateAdded = new Date(item.added_at);
        const currentAge = new Date().getTime() - dateAdded.getTime();
        const age_updated_item = ageTask(item.content, item.priority, currentAge, tAge, item.id, dateAdded);
        // return null if the aged item's values haven't changed
        if (age_updated_item.content === item.content && age_updated_item.priority === item.priority && _.isEqual(age_updated_item.due, item.due)) {
            return null;
        } else {
            return {
                type: "item_update",
                uuid: uuid(),
                args: age_updated_item
            }
        }
    } else {
        return null;
    }
    
}

export function getUpdateCommands(items_response: ItemsResponse): Array<ItemUpdateCommand> {
    // _.compact removes null items from the results
    return _.compact(_.map(items_response.items, singleItemUpdate));
}
