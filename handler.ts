import { ICallback, IEventPayload } from './model';
import { getUpdateCommands } from './age-util';
import { getItems, updateItems } from './todoist';

export function hello(event: IEventPayload, context, callback: ICallback) {
    getItems((res) => {
        updateItems(res.data.sync_token, getUpdateCommands(res.data));
    })
    callback(undefined, {
      message: "Aging your tasks.", 
      event 
    });
};
