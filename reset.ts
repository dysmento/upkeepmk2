import { TodoistApi } from '@doist/todoist-api-typescript'
import { ICallback, IEventPayload } from './model';

const api = new TodoistApi(process.env.TODOIST_TOKEN)
const upkeepProjectId = "2232783325"

export function handler(event: IEventPayload, context, callback: ICallback) {
    console.log(event);
    let body = JSON.parse(event.body);
    if (body.event_name === 'item:completed' && body.event_data.project_id === upkeepProjectId) {
      let assignee = body.initiator.id == "27505595" ? "27505627" : "27505595";
      var newTitle = body.event_data.content.startsWith('✅') ? body.event_data.content : '✅ ' + body.event_data.content;
      newTitle = newTitle.replace('☣️', '');
      newTitle = newTitle.replace('  ', ' ');
      api.addTask({
          content: newTitle,
          projectId: upkeepProjectId,
          assigneeId: assignee
      })
  }
    callback(undefined, { message: "Thank you, come again.", event: event})
}

